import bangunDatar.Persegi;
import bangunDatar.Segitia;

public class App {
    public static void main(String[] args) {
        System.out.println("testing");

        Persegi persegi = new Persegi();
        persegi.sisiPersegi = 20;
        persegi.cetakSisi();
        System.out.println("Get sisi:" + persegi.getSisi());
        persegi.cetakKeliling();
        System.out.println("Get Luas" + persegi.getLuas());
        /*
         * // SecondPersegi secondPersegi = new SecondPersegi();
         * gk jalan karena second persegi modifiernya default
         */

        // instance object segittiga
        Segitia segitiga = new Segitia();
        segitiga.setAlas(2);
        segitiga.setTinggi(5);
        segitiga.setSisiMiring(10);
        segitiga.cetakSegitiga(7);
        System.out.println("Alas Segitia:" + segitiga.getAlas());
        System.out.println("Tinggi Segitia:" + segitiga.getTinggi());
        System.out.println("luas:" + segitiga.getLuas());
        System.out.println("keliling:" + segitiga.getKeliling());

       

    }

}