package bangunDatar;

public class Segitia {
    /* modifier 
     * 1. public : semua attribute,class,method  bisa di panggil.
     * 2. default : hanya bisa di panggil di dalam class atau package.
     * 3. private : hanya bisa di akses dari file yang sama aja.
     * 4. protected: bisa di gunakan dalam class, package dan sub class yang sama.
    */

    // SecondPersegi sPersegi= new SecondPersegi();
    private int alas, tinggi,sisiMiring;

    // method setter&getter
    /*
     * setter untuk mengubah value
     * getter untuk mendapatakan value
     */
    // method Setter
    public void setAlas(int alaSegitiga){
        alas = alaSegitiga;
    }
    // method Getter
    public void setTinggi(int tinggi){
        this.tinggi = tinggi;
    }
    public void setSisiMiring(int sisiMiring){
        this.sisiMiring = sisiMiring;
    }

    public int getAlas(){
        return alas;
    }
    public int getSisiMiring(){
        return sisiMiring;
    }
    public int getTinggi(){
        return tinggi;
    }

    // method custom
    public int getKeliling(){
        int keliling = alas+tinggi+sisiMiring;
        return keliling;
    }
    public float getLuas(){
        float luas = alas*tinggi/2;
        return luas;
    }
    public void cetakSegitiga(int sisi){

        for (int i = 0; i < sisi; i++) {
            for (int j = 0; j < sisi; j++) {
                if (i>=j) {
                    
                    System.out.print("*");
                }
                
            }
            System.out.println();
        }
    }

}
