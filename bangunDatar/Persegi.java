package  bangunDatar;

import java.util.Scanner;

public class Persegi{
    public int sisiPersegi;

    //Method
    /* type
     * 1. void :tidak mengembalikan data
     * 2. return mengembalikan data
     * 
     */

     public void cetakSisi(){
        System.out.println(sisiPersegi);
     }
     public int getSisi(){
        return sisiPersegi;
     }
     public int getLuas(){
        return sisiPersegi*sisiPersegi;
     }
     public void cetakKeliling(){
        System.out.println("Keliling :"+ 4*sisiPersegi);
     }
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        
        /*
         * Scanner Class pembantu
         * input = nama object scanner
         * new proses untuk instance object baru
         * scanner() = constructor
         */

        int sisi, keliling,luas;
        String namaBangun = "Persegi";
        // sisi=20;
        System.out.println("Input sisi:");
        sisi = input.nextInt();
        keliling= 4* sisi;
        luas = sisi*sisi;

        System.out.println("====="+namaBangun+"======");
        System.out.println("""
                ini merupakan print untuk beberapa baris
                jadi kita coba aja
                semoga bisa
                                """);

        System.out.print("sisi:");
        System.out.println(sisi);
        System.out.print("hitung keliling:");
        System.out.println(keliling);
        System.out.print("hitung luas:");
        System.out.println(luas);
        SecondPersegi sPersegi= new SecondPersegi();
    }

}
class SecondPersegi{

}