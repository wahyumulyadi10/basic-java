package oop;

public class Lingkaran implements BangunDatar {
    private double jarijari;
    

    public Lingkaran() {
    }

    
    public Lingkaran(double jarijari) {
        this.jarijari = jarijari;
    }


    public double getJarijari() {
        return jarijari;
    }

    public void setJarijari(double jarijari) {
        this.jarijari = jarijari;
    }
    
    @Override
    public void printShape() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public double hitungKeliling() {
        double keliling =  2 * Math.PI *jarijari;

        // TODO Auto-generated method stub
        return keliling;
    }

    @Override
    public double hitungLuas() {
        Double luas = Math.PI *jarijari*jarijari;
        // TODO Auto-generated method stub
        return luas;
    }

    
}
