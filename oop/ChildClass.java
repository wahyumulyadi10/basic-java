package oop;

/* Inheritance
 * suatu class dapat di wariskan ke class lainnya
 * 1. extends: class-class atau interface - interface
 * 2. implement: interface -class
 */
public class ChildClass extends ParentClass{
    private String kalimat;
    
    /*
     * 1. super representasi dari parent class
     * 2. this  representasi dari classnya sendiri
     */
    

    public ChildClass(String tulisan, int nummber, String kalimat) {
        super(tulisan, nummber);
        super.setTeks(kalimat);
        this.kalimat = tulisan;
        this.setKalimat(tulisan);
    }

    public ChildClass() {

    }

    public String getKalimat() {
        return kalimat;
    }

    public void setKalimat(String kalimat) {
        this.kalimat = kalimat;
    }

    @Override
    public void greeting(String sapa){
        System.out.println(sapa +"dari child");
    }
    
}
