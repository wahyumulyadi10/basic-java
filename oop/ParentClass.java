package oop;

public class ParentClass {
    private String teks;
    private int angka;
        // default constructor
        public ParentClass(){

        }
        // consturctor with params
        public ParentClass(String tulisan,int nummber){
            this.teks = tulisan;
            this.angka = nummber;
        }
        public ParentClass(String tulisan,int nummber, String contoh){
            this.teks = tulisan;
            this.angka = nummber;
        }

    public String getTeks() {
        return teks;
    }
    /* 
     * Polymorphism , banyak bentuk
     * 1. overload, nama nya sama namun params/argumennya beda sehingga di bedakan dari paramsnya
     * 2. 
     */
    public String getTeks(String text) {
        System.out.println("ini text dari getText with params " +text);
        return teks;
    }
    public void setTeks(String teks) {
        this.teks = teks;
    }
    public int getAngka() {
        return angka;
    }
    public void setAngka(int angka) {
        this.angka = angka;
    }
    public void greeting(String sapa){
        System.out.println(sapa+ "dari parent");
    }
}
