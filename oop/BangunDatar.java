package oop;
/*
 * abstraction
 * 1. gambar bangun
 * 2. hitung keliling
 * 3. hitung luas
 */
public interface BangunDatar {
    void printShape();
    double hitungKeliling();
    double hitungLuas();

}
