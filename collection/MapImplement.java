package collection;

import java.util.HashMap;
import java.util.Map;

public class MapImplement {
    public static void main(String[] args) {
        Map<String, Integer> stock = new HashMap();
        stock.put("k1", 1);
        stock.put("k2", 2);
        stock.put("k3",3);
        stock.put("k4", 4);
        stock.put("k5", 5);
        stock.put("k6", 6);
        System.out.println(stock);

        // get
        System.out.println(stock.get("k1"));
        // update
        stock.replace("k1", 1, 5);
        stock.replace("k6",  10);
        System.out.println(stock);
        // remove
        stock.remove("k1");
        // clear semua hasmap
        System.out.println(stock);
        stock.clear();
        System.out.println(stock);
        
    }
}
