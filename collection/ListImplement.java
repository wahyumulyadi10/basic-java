package collection;

import java.util.ArrayList;
import java.util.List;

public class ListImplement {
    public static void main(String[] args) {
        
        List<String>toDo = new ArrayList<>();

        toDo.add("makan");
        toDo.add("tidur");
        toDo.add("makan");
        toDo.add( "belajar");
        System.out.println(toDo);
        // get DAta
            System.out.println(toDo.get(1));
        // update
        toDo.set(0, "tidur");
        System.out.println(toDo);
        // delete
        toDo.remove(0);
        toDo.remove("belajar");
    
        System.out.println(toDo);
    }
}
