import oop.BangunDatar;
import oop.ChildClass;
import oop.DateImplement;
import oop.Lingkaran;
import oop.ParentClass;

public class Main {
    public static void main(String[] args) {
        ParentClass parent = new ParentClass();
        parent.setTeks("hallow");
        parent.setAngka(100);

        System.out.println("parent :"+parent.getAngka()+""+parent.getTeks());
        parent = new ParentClass("world", 1000);

        ChildClass cClass = new ChildClass();
        cClass.setAngka(200);
        cClass.setTeks("hallo");
        cClass.setKalimat("aaa");

        System.out.println("parent :"+parent.getAngka()+""+parent.getTeks());
        System.out.println("cCLass :"+cClass.getAngka()+""+cClass.getTeks());
        System.out.println(parent.getTeks("helloooooooooooo"));
        cClass.greeting("assa");


        ChildClass childClass = new ChildClass();
        System.out.println("get attribute from parent"+childClass.getAngka());
        ChildClass childClass2= new ChildClass("hallo", 20, "sooo");
        System.out.println(childClass2.getAngka());



        BangunDatar lingkaran = new Lingkaran(7);
        System.out.println("luas : "+ lingkaran.hitungLuas());
        System.out.println("keliling : "+ lingkaran.hitungKeliling());

        DateImplement dateImplement = new DateImplement();
        DateImplement.contoh ="tulisan";
        // DateImplement.main(args);
    }
}
